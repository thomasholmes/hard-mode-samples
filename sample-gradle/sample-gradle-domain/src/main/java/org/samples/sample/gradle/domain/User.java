package org.samples.sample.gradle.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class User {

    /** First name. */
    private String firstName;

    /** Last name. */
    private String lastName;

    /**
     * Gets first name.
     * 
     * @return first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     * 
     * @param firstName
     *            The first name.
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     * 
     * @return last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     * 
     * @param lastName
     *            The last name.
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(firstName).append(lastName).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append(firstName).append(lastName).toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        final User otherUser = (User) obj;
        return new EqualsBuilder().append(firstName, otherUser.firstName).append(lastName, otherUser.lastName)
                .isEquals();
    }

}