package org.samples.sample.gradle.service;

import org.samples.sample.gradle.domain.User;

public class Sample {

    public static void main(final String[] args) {

        final User user = new User();
        user.setFirstName("User");
        user.setLastName("Userington");
        
        System.out.println(String.format("Hello, %s %s", user.getFirstName(), user.getLastName()));
    }
}