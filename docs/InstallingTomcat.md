# Installing Tomcat 7

*Note that this guide was written for Windows. Please update with any other OS specific info.*

This guide assumes Java is already installed.

## Getting Tomcat

Tomcat is available from the [Apache website](https://tomcat.apache.org/download-70.cgi). Windows users should get
the x64 zip. *Mac OS X should probably get just the zip.*

Unzip the archive to somewhere where programs can be run from. For Windows this could be in C:\Program Files. Note that war files will be deployed here so it should be somewhere with sufficient space.

*For Windows this folder needs to be granted full control permissions for non admin users. Not sure if similar steps are required for Mac. In either case, full control IS A VERY BAD IDEA FOR PRODUCTION SERVERS. We'll get over it for development though.*

We need to set up permission for deployment. This is managed in TOMCAT_DIRECTORY/conf/tomcat-users.xml.

Copy the following xml into the tomcat-users tag:
```xml
<tomcat-users>
  ...
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <user username="tomcat" password="tomcat" roles="manager-script,manager-gui"/>
  ...
</tomcat-users>
```
*Note that the username and password here are tomcat. This can be changed, but you should also change to credential in the root build.gradle to match.*

To start Tomcat start a terminal in TOMCAT_DIRECTORY/bin and run the following command:
```
catalina jpda start
```

This should open a Java console window which logs the Tomcat server. Close this window to kill the server. You can test Tomcat by going to http://localhost:8080.
